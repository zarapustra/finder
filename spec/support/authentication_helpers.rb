module AuthenticationHelpers
  def sign_in_as!(user)
    visit '/users/sign_in'
    fill_in "Email", with: user.email
    fill_in t('activerecord.attributes.user.password'), with: user.password
    click_button t'devise.sessions.new.sign_in'
    expect(page).to have_content(t('devise.sessions.signed_in'))
  end
  def sign_out(user)!
    visit destroy_user_session_path(I18n.locale,user)
    expect(page).to have_content(t('devise.sessions.signed_out'))
  end
end
RSpec.configure do |c|
  c.include AuthenticationHelpers, type: :feature
end

module AuthHelpers
  def sign_in(user)
    session[:user_id] = user.id
  end
end
RSpec.configure do |c|
  c.include AuthHelpers, type: :controller
end
