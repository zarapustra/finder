# -*- encoding : utf-8 -*-
RSpec.configure do |config|
  def t(string, options={})
    I18n.t(string, options)
  end
end
