# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Deleting Questions' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, :user_id => user.id) }
  let!(:answer) { FactoryGirl.create(:answer, question_id: question.id, :user_id => author.id) }

  describe "signed in as autor can delete his answer" do
    before do
      sign_in_as!(author)
      visit question_path(@locale, question)
      click_link "delete_#{answer.id}"
    end
    scenario "and there is no more answer on question page" do
      expect(page).to have_no_content('Near the central park')
      expect(page).to have_content(t('answers.deleted'))
    end

    scenario "and there is no more answer in author's profile" do
      visit user_path(@locale, author)
      expect(page).to have_no_content('Near the central park')
    end
  end

  describe "signed in as a side user" do
    scenario "can't see deletion link" do
      sign_in_as!(user)
      visit question_path(@locale, question)
      within "#answer_#{answer.id}" do
        expect(page).to have_no_content(t('answers.delete_link'))
      end
    end
  end
end
