# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Creating Questions' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, :user_id => user.id) }
  before do
    sign_in_as!(user)
    visit '/'
  end
  context "In ru locale" do
    scenario "can create question" do
      click_link t 'questions.index.add_question'
      fill_in 'question_body', with: 'Where to buy a bicycle?'
      fill_in 'question_description', with: 'For a kid'
      click_button t('questions.new.submit')
      expect(page).to have_content(t 'questions.created')
      question = Question.where(body: "Where to buy a bicycle?").first
      expect(page.current_url).to eql(question_url(I18n.locale, question.id))
    end

    scenario "can not create a question without a body" do
      click_link t 'questions.index.add_question'
      click_button t('questions.new.submit')
      expect(page).to have_content(t 'questions.not_created')
    end

    scenario "can see question's author" do
      within "##{question.id}" do
        expect(page).to have_content(user.name)
      end
    end
  end
  context "In en locale" do
    scenario "can't see question that was created with other @locale" do
      click_link 'English'
      expect(page).to have_no_content(question.body)
    end
  end

end
