# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Deleting Questions' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, :user_id => author.id) }

  describe "signed in as autor can delete question" do
    before do
      sign_in_as!(author)
      visit question_path(@locale, question)
      click_link 'deleteQuestion'
    end
    scenario "and there is no more question on home page" do
      expect(page).to have_no_content('Where to buy a pumpkin?')
      expect(page).to have_content(t('questions.deleted'))
    end
    scenario "and there is no more question in search results" do
      fill_in 'query', with: 'pumpkin'
      click_button 'submit'
      within '#foundQuestions' do
        expect(page).to have_no_content('Where to buy a pumpkin?')
      end
    end
    scenario "and there is no more question in user profile" do
      visit user_path(@locale, author)
      expect(page).to have_no_content('Where to buy a pumpkin?')
    end
  end

  describe "signed in as a side user" do
    scenario "can't see deletion link" do
      sign_in_as!(user)
      visit question_path(@locale, question)
      within '#questionInfoLine' do
        expect(page).to have_no_content(t('questions.show.delete'))
      end
    end
  end
end
