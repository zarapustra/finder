# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Creating Answers' do
  let(:user){FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, user_id: user.id) }

  before do
    sign_in_as!(user)
    visit '/'
    click_link question.body

  end
  scenario "can create answer" do

    fill_in 'answer_body', with: 'On a market'
    click_button t('answers.new.submit')

    expect(page).to have_content(t('answers.created'))
    expect(page).to have_content('On a market')
    visit '/'
    within "##{question.id} .answersCount" do
      expect(page).to have_content(1)
    end
  end

  scenario "can not create an answer without a body" do

    click_button t('answers.new.submit')

    expect(page).to have_content(t('answers.not_created'))
    #expect(page).to have_content(t('errors.messages.not_saved',count: 1, resource: t('activerecord.models.answer')))
    #save_and_open_page
  end
end
