# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Editing Answers' do
  let(:user) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, user_id: user.id) }
  let!(:answer) { FactoryGirl.create(:answer, question_id: question.id, user_id: author.id) }

  scenario "can edit and update answer" do
    sign_in_as!(author)
    visit question_path(I18n.locale, question)
    click_link "edit_#{answer.id}"
    fill_in 'answer_body', with: "In central shop"
    click_button t('answers.edit.submit')

    expect(page).to have_content(t('answers.updated'))
    expect(page).to have_content('In central shop')
  end

  scenario "can't see edit link if not author" do
    sign_in_as!(user)
    visit question_path(I18n.locale, question)
    within "#answer_#{answer.id}" do
      expect(page).to have_no_content(t 'answers.edit.title')
    end
  end

  scenario "can't edit question if not author" do
    sign_in_as!(user)
    visit edit_answer_path(I18n.locale, answer)
    expect(page.current_url).to eql(question_url(I18n.locale, question.id))
    expect(page).to have_content(t 'errors.no_access')
  end
end