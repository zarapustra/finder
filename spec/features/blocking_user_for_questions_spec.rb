# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Blocking User for Questions' do
  let(:admin) { FactoryGirl.create(:admin_user) }
  let(:reader) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: author.id, spam_counter: 9) }

  describe "admin user" do
    scenario "can see if question's author was blocked" do
      sign_in_as!(admin)
      visit question_path(@locale, question)
      click_link t('spam')
      visit admin_root_path(@locale)
      expect(page).to have_content(author.name)
    end

  end

  describe "reader" do
    before do
      sign_in_as!(reader)
      visit question_path(@locale, question)
      click_link t('spam')
    end
    scenario "can see user profile blocked" do
      visit user_path(@locale, author)
      expect(page).to have_content(t('users.show.blocked'))

    end
    scenario "can see blocked question disapears from main page" do
      visit '/'
      expect(page).to have_no_content(question.body)
    end

    scenario "can't find question with search field" do
      visit '/'
      fill_in 'query', with: 'pumpkin'
      click_button 'submit'
      within '#foundQuestions' do
        expect(page).to have_no_content('Where to buy a pumpkin?')
      end
    end
  end
end
