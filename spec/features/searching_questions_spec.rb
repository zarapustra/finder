# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Searching questions' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, :user_id => user.id) }
  before do
    visit '/'
  end
  scenario 'can search recently created question' do
    fill_in 'query', with: 'pumpkin'
    click_button 'submit'
    within '#foundQuestions' do
      expect(page).to have_content('Where to buy a pumpkin?')
    end

  end
end