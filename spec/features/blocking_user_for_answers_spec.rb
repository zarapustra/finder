# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Blocking User for Answers' do
  let(:admin) { FactoryGirl.create(:admin_user) }
  let(:reader) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: reader.id) }
  let!(:answer) { FactoryGirl.create(:answer, user_id: author.id, question_id: question.id, spam_counter: 9) }

  describe "admin user" do
    before do
      sign_in_as!(admin)
    end
    scenario "can see if answer's author was blocked" do
      visit question_path(@locale, question)

      within "#answer_#{answer.id}" do
        click_link t('spam')
      end
      visit admin_root_path(@locale)
      expect(page).to have_content(author.name)
    end
  end

  describe "reader" do
    before do
      sign_in_as!(reader)
      visit question_path(@locale, question)
      within "#answer_#{answer.id}" do
        click_link t('spam')
      end
    end
    scenario "can see user profile blocked" do
      visit user_path(@locale, author)
      expect(page).to have_content(t('users.show.blocked'))

    end
    scenario "can see blocked answer disapears from question page" do
      visit question_path(@locale, question)
      expect(page).to have_no_content(answer.body)
    end
  end
end
