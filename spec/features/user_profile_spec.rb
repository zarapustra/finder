require 'rails_helper'

feature "Profile page" do
  scenario "viewing" do
    user = FactoryGirl.create(:user)
    sign_in_as!(user)
    visit user_path(I18n.locale, user)
    expect(page).to have_content(user.name)
    expect(page).to have_content(user.email)

  end
  let(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, body: "Where to buy plutonium?", user_id: author.id) }
  scenario "when smbd add question then users can access it through his profile" do
    visit user_path(I18n.locale, author)
    within "#userQuestions" do
      expect(page).to have_content(question.body)
    end
  end
end
feature "Editing Users" do
  scenario "Updating a profile without changing password" do
    user = FactoryGirl.create(:user)
    sign_in_as!(user)
    visit user_path(I18n.locale, user)
    click_link t('users.edit')
    expect(page).to have_content(t('activerecord.attributes.user.name'))
    fill_in 'user_name', with: "Kim Chen In"
    fill_in 'user_current_password', with: user.password
    click_button t('devise.registrations.edit.update')
    expect(page).to have_content(t('devise.registrations.updated'))
  end
end
