# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Liking answers' do
  let(:sideUser) { FactoryGirl.create(:user) }
  let(:answerAuthor) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: sideUser.id) }
  let!(:answer) { FactoryGirl.create(:answer, question_id: question.id, user_id: answerAuthor.id) }

  describe "side user" do
    before do
      sign_in_as!(sideUser)
      visit question_path(@locale, question)
    end

    scenario "can like answer" do
      click_link t('answers.like')
      expect(page).to have_link(t('answers.liked'))
      within (".likesCount") do
        expect(page).to have_content(1)
      end
      click_link t('answers.liked')
      expect(page).to have_no_css('span.likesCount')
    end
  end

  describe "answer's author" do
    scenario "cannot like his answer" do
      sign_in_as!(answerAuthor)
      visit question_path(@locale, question)
      click_link t('answers.like')
      expect(page).to have_content(t('answers.errors.author_vote'))
    end
  end
end
