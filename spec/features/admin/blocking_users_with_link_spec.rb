# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Blocking User with Link' do
  let(:admin) { FactoryGirl.create(:admin_user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: author.id) }
  let!(:answer) { FactoryGirl.create(:answer, user_id: author.id, question_id: question.id) }

  before do
    sign_in_as!(admin)
  end

  scenario "can click block link on user's profile page" do
    visit user_path(@locale, author)
    click_link 'Блокировать'
    expect(page).to have_content(t('users.show.blocked'))
  end

  scenario "can click block link on user's question page" do
    visit question_path(@locale, question)
    within "#questionInfoLine" do
      click_link 'Блокировать'
    end
    visit user_path(@locale, author)
    expect(page).to have_content(t('users.show.blocked'))
  end

  scenario "can click block link on user's answer" do
    visit question_path(@locale, question)
    within "#answer_#{answer.id}" do
      click_link 'Блокировать'
    end
    visit user_path(@locale, author)
    expect(page).to have_content(t('users.show.blocked'))
  end
end
