# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Unblocking User with Link' do
  let(:admin) { FactoryGirl.create(:admin_user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: author.id) }
  let!(:answer) { FactoryGirl.create(:answer, user_id: author.id, question_id: question.id, spam_counter: 9) }

  before do
    sign_in_as!(admin)
  end

  scenario "unblock link markin blocked question or answer as deleted" do
    visit question_path(@locale, question)
    within "#answer_#{answer.id}" do
      click_link t('spam')
    end
    visit user_path(@locale, author)
    click_link 'Разблокировать'
    expect(page).to have_content(t('users.active'))
    expect(answer.reload).to have_attributes(deleted: true)
  end

end
