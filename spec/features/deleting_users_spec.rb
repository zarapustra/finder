# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Deleting Users' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:friend) { FactoryGirl.create(:user) }

  before do
    sign_in_as!(user)
  end
  scenario "user deletes himself" do
    visit edit_user_registration_path(@locale, user)
    click_link t('devise.registrations.edit.cancel_my_account')
    visit '/users/sign_in'
    fill_in "Email", with: user.email
    fill_in t('activerecord.attributes.user.password'), with: user.password
    click_button t'devise.sessions.new.sign_in'
    expect(page).to have_content(t('devise.failure.deleted_account'))

    sign_in_as!(friend)
    visit user_path(@locale, user)
    expect(page).to have_no_content(user.name)
    expect(page).to have_content(t'users.deleted')
  end

end