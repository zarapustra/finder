# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Subscribing question' do
  let(:user) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, user_id: author.id) }

  describe "side user" do
    before do
      sign_in_as!(user)
      visit question_path(@locale, question)
      click_link t('questions.show.subscribe')
    end
    scenario "can subscribe and unsubscribe question" do
      expect(page).to have_content(t('questions.show.subscribed'))
      within ".watchersCount" do
        expect(page).to have_content(2)
      end
      click_link t('questions.show.subscribed')
      expect(page).to have_content(t('questions.show.subscribe'))
      within ".watchersCount" do
        expect(page).to have_content(1)
      end
    end
    scenario "subscribes question and can see it in his profile" do
      visit user_path(@locale, user)
      within "#userSubs" do
        expect(page).to have_content(question.body)
      end
    end
  end
  describe "author" do
    scenario "subscribes questions after it's creation" do
      sign_in_as!(author)
      visit question_path(@locale, question)
      expect(page).to have_content(t 'questions.show.subscribed')
      within ".watchersCount" do
        expect(page).to have_content(1)
      end
    end
  end
end