# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Marking Answers as Spam' do
  let(:user) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, user_id: user.id) }
  let!(:answer) { FactoryGirl.create(:answer, question_id: question.id, user_id: author.id) }
  let(:admin_user) { FactoryGirl.create(:admin_user) }


  describe "answer's author" do
    before do
      sign_in_as!(author)
    end

    scenario "can't see spam link" do
      visit question_path(@locale, question)
      within '#answers' do
        expect(page).to have_no_link(t('spam'))
      end
    end

  end

  describe "side user" do
    before do
      sign_in_as!(user)
    end
    scenario "can mark answer like spam" do
      visit question_path(@locale, question)
      click_link t 'spam'
      within '#answers' do
        expect(page).to have_content(t 'spammed')
      end
    end
  end

  describe "admin user" do
    scenario "can see author of answers marked as spam" do
      sign_in_as!(admin_user)
      visit mark_as_spam_answer_path(@locale, answer)
      visit admin_root_path(@locale)
      expect(page).to have_content(author.name)
      expect(page).to have_content(answer.body)
    end
  end

end
