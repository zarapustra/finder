# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Editing Questions' do
  let(:user) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let!(:question) { FactoryGirl.create(:question, user_id: author.id) }

  describe "author can edit question" do
    before do
      sign_in_as!(author)
      visit edit_question_path(I18n.locale, question)
      fill_in 'question_body', with: "Where can i buy a prostitute?"
      fill_in 'question_description', with: "I need a hot one with cool boobs"
      click_button t('questions.edit.submit')
    end
    scenario "and can see changes on question page" do
      expect(page).to have_content(t('questions.updated'))
      expect(page).to have_content('Where can i buy a prostitute?')
      expect(page).to have_content('I need a hot one with cool boobs')
    end
    scenario "and can find updated question" do
      visit('/')
      fill_in 'query', with: 'prostitute'
      click_button 'submit'
      within '#foundQuestions' do
        expect(page).to have_content('Where can i buy a prostitute?')
      end
    end
  end
  describe "side user" do
    before do
      sign_in_as!(user)
    end
    scenario "can't see edit link" do
      visit question_path(I18n.locale, question)
      expect(page).to have_no_content(t 'questions.show.edit')
    end

    scenario "can't edit question" do
      visit edit_question_path(I18n.locale, question)
      expect(page.current_url).to eql(question_url(I18n.locale, question.id))
      expect(page).to have_content(t 'errors.no_access')
    end
  end
end