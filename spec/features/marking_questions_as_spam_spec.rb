# -*- encoding : utf-8 -*-
require 'rails_helper'

feature 'Marking Questions as Spam' do
  let(:user) { FactoryGirl.create(:user) }
  let(:author) { FactoryGirl.create(:user) }
  let(:admin_user) { FactoryGirl.create(:admin_user) }
  let!(:question) { FactoryGirl.create(:question, user_id: author.id) }

  describe "question's author" do
    before do
      sign_in_as!(author)
    end

    scenario "can't see spam link" do
      visit question_path(@locale, question)
      within '#questionInfoLine' do
        expect(page).to have_no_link(t('spam'))
      end
    end

  end

  describe "side user" do
    before do
      sign_in_as!(user)
    end
    scenario "can mark question like spam" do
      visit question_path(@locale, question)
      click_link t 'spam'
      within '#questionInfoLine' do
        expect(page).to have_content(t 'spammed')
      end
    end
  end

  describe "admin user" do
    scenario "can see author of questions marked as spam" do
      sign_in_as!(admin_user)
      visit mark_as_spam_question_path(@locale, question)
      visit admin_root_path(@locale)
      expect(page).to have_content(author.name)
      expect(page).to have_content(question.body)
    end
  end

end
