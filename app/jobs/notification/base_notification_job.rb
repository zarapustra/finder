class Notification::BaseNotificationJob < ApplicationJob
  queue_as :notification

  def perform(options)
    service_object_class.new(options).run
  end

  private

  def service_object_class_name
    self.class.name.underscore.split('/')[1..-1].join('_').sub('_job', '')
  end

  def service_object_class
    "Notification::#{service_object_class_name.classify}".constantize
  end
end
