class ApplicationJob < ActiveJob::Base
  include Rails.application.routes.url_helpers

  protected

  def default_url_options
    ActionMailer::Base.default_url_options
  end

  def full_url(relative_url) # TODO: find this damn method in rails' code base!
    return '' unless relative_url
    relative_url = relative_url.gsub(/^./, '') if relative_url.starts_with? '/'
    port = default_url_options[:port]
    host = default_url_options[:host]
    protocol = default_url_options[:protocol] || 'http'
    "#{protocol}://#{host}#{port ? ":#{port}" : ''}/#{relative_url}"
  end

  def with_user_locale(user)
    I18n.with_locale(user.locale) do
      yield
    end
  end
end
