# -*- encoding : utf-8 -*-
class QuestionsController < ApplicationController
  include CVisitsCountable
  include CSubscribeable
  before_action :set_question, only: [:edit,
                                      :update,
                                      :destroy,
                                      :mark_as_spam,
                                      :subscribe]
  before_action :authenticate_user!, except: [:show, :search, :autocomplete]

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    @question.user_id=current_user.id

    if @question.save
      flash[:notice] = t('questions.created')
      redirect_to @question
    else
      flash[:alert] = t('questions.not_created')
      render 'new'
    end
  end

  def show
    question = Question.find(params[:id])
    if !question.blocked?
      answers = Answer.active.includes(:user)
      .where(answers: { question_id: params[:id] })
      .order(created_at: :desc)
      .page(params[:page])
      @answers = cell(:answer, collection: answers).()
    else
      flash[:alert]=t('errors.no_access')
      redirect_to :root
    end
    @question = cell(:question, question).(:show, answers: @answers, answer: question.answers.build)
  end

  def edit
    if current_user != @question.user
      flash[:alert]=t('errors.no_access')
      redirect_to @question
    end

  end

  def update
    if @question.update(question_params)
      flash[:notice] = t('questions.updated')
      redirect_to @question
    else
      flash[:alert] = t('questions.not_updated')
      render "edit"
    end
  end

  def destroy
    if current_user == @question.user
      @question.update_attribute(:deleted, true)
      flash[:notice] = t('questions.deleted')
      redirect_to :root
    else
      flash[:alert]=t('errors.no_access')
      redirect_to :back
    end
  end

  def mark_as_spam
    if @question.claimed_by?(current_user)
      flash[:alert]=t 'spammed'
    else
      @question.create_claim_by(current_user)
      flash[:notice]=t('spammed')
    end
    redirect_to :back
  rescue ActionController::RedirectBackError
    redirect_to :root
  end

  def subscribe
    if @question.subscribed_by?(current_user)
      unsubscribe_it
    else
      subscribe_it
    end
    redirect_to :back
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  def search
    if not params[:query].blank?
      @foundQuestions=Question.active.search params[:query], limit: 10
    end
    @questions=Question.active.where(:locale => I18n.locale.to_s).page(params[:page])
    render :js
  end

  def autocomplete
    render json: Question.active.search(params[:query], {
        fields: ["body^5", "description"],
        limit: 10,
        load: false,
        misspellings: {below: 5}
    }).map { |question| {body: question.body, id: question.id} }
  end
end

private

def question_params
  params.require(:question).permit(:body, :description, :user_id, :image, :remote_image_url, :image_cache)
end

def set_question
  @question = Question.find(params[:id])
end
