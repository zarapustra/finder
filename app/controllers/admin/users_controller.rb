# -*- encoding : utf-8 -*-
class Admin::UsersController < ApplicationController
  before_action :set_user

  def show
    @active_questions = @user.questions.active.limit(10)
    @active_answers = @user.answers.active.limit(10)
  end

  def block
    if @user.admin
      flash[:alert]="Нельзя заблокировать администратора."
    else
      @user.block_it
    end
    redirect_to :back

  end

  def unblock
    @user.unblock_it
    redirect_to :back

  end

  private

  def set_user
    @user= User.find(params[:id])
  end
end