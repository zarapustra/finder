# -*- encoding : utf-8 -*-
class Admin::BaseController < ApplicationController
  before_filter :authenticate_admin!

  def index
    @spam_level=Setting.spam_level
    @blocked_users=User.blocked
    @claimed_questions = Question.claimed
    @claimed_answer = Answer.claimed
  end

  def change_spam_level
    if params[:spam_level]
      Setting.spam_level = params[:spam_level]
    end
    redirect_to :index
  end
end
