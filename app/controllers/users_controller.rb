# -*- encoding : utf-8 -*-
class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
    case params[:tab]
      when 'answers'
        #@answers=@user.answers.with_active_questions.page(params[:page])
        @answers=Answer.includes(:user, :question)
        .where(questions: { deleted: false, blocked: false })
        .where(answers: { deleted: false, blocked: false })
        .where(users: { id: params[:id] }).page(params[:page])
        @tab='answers'
      when 'subscriptions'
        @subscriptions=@user.subscribed_questions.active.page(params[:page])
        @tab='subscriptions'
      else
        @questions=@user.questions.active.page(params[:page])
        @tab='questions'
    end
  end

  def feed

  end
end
