# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery :with => :exception
  include ApplicationHelper
  before_action :set_locale
  before_filter :blocked?, :restrict_actions

  private

  def set_locale
    if params[:locale].present?
      I18n.locale= params[:locale]
    else
      I18n.locale= user_signed_in? ? current_user.locale : I18n.default_locale
    end
  end

  def default_url_options(options={})
    {:locale => I18n.locale}
  end


  def blocked?
    if user_signed_in? && current_user.blocked?
      sign_out current_user
      flash[:error] = "This account has been suspended...."
      root_path
    end
  end

  def restrict_actions
    @restrict = user_signed_in? ? '' :  'modalable'
  end
end
