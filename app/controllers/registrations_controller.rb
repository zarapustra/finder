class RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters

  def destroy
    resource.soft_delete
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    set_flash_message :notice, :destroyed if is_flashing_format?
    yield resource if block_given?
    respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :locale, :avatar, :remote_avatar_url, :avatar_cache) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :locale, :avatar, :remote_avatar_url, :avatar_cache) }
    end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end
end
