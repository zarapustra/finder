# -*- encoding : utf-8 -*-
class AnswersController < ApplicationController
  include CLikeable
  before_filter :find_question, except: [:destroy]
  before_filter :find_answer, except: [:create]

  def create
    @answer = @question.answers.build(answer_params)
    @answer.user = current_user
    if @answer.save
      Notification::QuestionAnswered.new(answer: @answer).run
      flash[:notice] = t 'answers.created'
      redirect_to @answer.question
    else
      flash[:alert] = t 'answers.not_created'
      redirect_to :back
    end
  end

  def edit
    if current_user!= @answer.user
      flash[:alert]=t('errors.no_access')
      redirect_to @question
    end
  end

  def update
    if @answer.update(answer_params)
      flash[:notice] = t('answers.updated')
      redirect_to @question
    else
      flash[:alert] = t('answers.not_updated')
      render "edit"
    end
  end

  def destroy
    if current_user== @answer.user
      @answer.deleted=true
      @answer.save
      flash[:notice] = t('answers.deleted')
    else
      flash[:alert]=t('errors.no_access')
    end
    redirect_to :back
  end

  def mark_as_spam
    if @answer.claimed_by?(current_user)
      flash[:alert]=t 'spammed'
    else
      @question.create_claim_by(current_user)
      flash[:notice]=t('spammed')
    end
    redirect_to :back
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  def like
    if @answer.user==current_user
      flash[:alert]=t('answers.errors.author_vote')
    else
      if @answer.liked_by?(current_user)
        dislike_it
      else
        like_it
      end
    end
    redirect_to :back
  rescue ActionController::RedirectBackError
    redirect_to root_path
  end

  def vote_up
    if @answer.user==current_user
      flash[:alert]=t('answers.errors.author_vote')
    else
      if @answer.voted_up_by?(current_user)
        # unvote it
        @answer.votes.where(user: current_user, positive: true).destroy_all
      else
        # vote it up
        @answer.votes.where(user: current_user, positive: false).destroy_all
        @answer.votes.create(user: current_user, positive: true)
      end
    end
    respond_to do |f|
      f.js { render layout: false }
    end
  end

  def vote_down
    if @answer.user==current_user
      flash[:alert]=t('answers.errors.author_vote')
    else
      if @answer.voted_down_by?(current_user)
        # unvote it
        @answer.votes.where(user: current_user, positive: false).destroy_all
      else
        # vote it down
        @answer.votes.where(user: current_user, positive: true).destroy_all
        @answer.votes.create(user: current_user, positive: false)
      end
    end
    respond_to do |f|
      f.js { render layout: false }
    end
  end

  private

  def answer_params
    params.require(:answer).permit(:body, :map, :question_id, :map, :remote_map_url, :map_cache)
  end

  def find_question
    if params[:question_id]
      @question = Question.find(params[:question_id])
    else
      find_answer
      @question = @answer.question
    end
  end

  def find_answer
    @answer = Answer.find(params[:id])
  end
end
