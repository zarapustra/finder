//= require jquery2
//= require jquery-ui
//= require jquery_ujs
//= require handlebars-v4.0.5.js
//= require semantic_ui/semantic_ui
//= require jquery.remotipart
//= require_tree .

$(document).ready(function() {
  $('.tabular.menu .item').tab({history: false});
});