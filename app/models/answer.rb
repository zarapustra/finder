# -*- encoding : utf-8 -*-
class Answer < ActiveRecord::Base
  include MClaimable
  include MBlockable
  include MLikeable
  mount_uploader :map, MapUploader

  belongs_to :question, inverse_of: :answers
  belongs_to :user, inverse_of: :answers
  alias_attribute :author, :user
  has_many :claims, as: :claimable
  has_one :block, as: :blockable
  has_many :votes, as: :votable

  validates :body, :presence => true

  scope :active, -> { where(deleted: false).not_blocked.order(created_at: :desc) }
  scope :with_active_questions, -> { active.includes(:question).where(deleted: false, blocked:false)}
  scope :blocked, -> { joins(:block).where("blocks.blockable_id = id and blocks.blockable_type = 'Answer'") }

  def votes_count
    votes.up.size - votes.down.size
  end

  def voted_down_by?(user)
    user.votes.merge(self.votes.down).length > 0
  end

  def voted_up_by?(user)
    user.votes.merge(self.votes.up).length > 0
  end
end
