# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  include MBlockable
  mount_uploader :avatar, AvatarUploader
  has_many :answers, inverse_of: :user
  has_many :questions, inverse_of: :user
  has_many :claims
  has_many :claimed_answers, through: :claims, source: :claimable, source_type: 'Answer'
  has_many :claimed_questions, through: :claims, source: :claimable, source_type: 'Question'
  has_one :block, as: :blockable
  has_one :settings_list, inverse_of: :user
  has_many :votes

  validates :name, :presence => true, unless: Proc.new { |a| a.name =='deleted' }
  before_save :set_locale

  def block_it
    Block.create(blockable: self)
    questions.each do |q|
      Block.create(blockable: q)
    end
    answers.each do |a|
      Block.create(blockable: a)
    end
  end

  def unblock_it
    block.destroy
    questions.each do |q|
      q.block && q.block.destroy
      q.claims.destroy_all
    end
    self.answers.each do |a|
      a.block && a.block.destroy
      a.claims.destroy_all
    end
  end

  def soft_delete
    update_attribute(:deleted_at, Time.current)
    update_attribute(:name, 'deleted')
  end

  # ensure user account is active
  def active_for_authentication?
    super && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    !deleted_at ? super : :deleted_account
  end

  def status
    if deleted_at.blank?
      if blocked?
        I18n.t('users.status.blocked')
      else
        I18n.t('users.status.active')
      end
    else
      I18n.t('users.status.deleted')
    end
  end

  def reindex_questions
    Question.reindex
  end

  private

  def set_locale
    self.locale=I18n.locale
  end
end
