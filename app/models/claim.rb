# -*- encoding : utf-8 -*-
class Claim < ActiveRecord::Base
  belongs_to :claimable, polymorphic: true
  belongs_to :user

  after_create :check_claims_count

  def check_claims_count
    if claimable.claims.length > Setting.spam_level.to_i
      claimable.user.block_it
    end
  end
end