class SettingsList < ActiveRecord::Base
  belongs_to :user, inverse_of: :settings_list
end