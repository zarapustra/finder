# -*- encoding : utf-8 -*-
class Question < ActiveRecord::Base
  include MBlockable
  include MSubscribeable
  include MVisitsCountable
  include MClaimable
  searchkick match: :word_start, searchable: [:body, :description]
  validates :body, presence: true
  belongs_to :user, inverse_of: :questions
  has_many :answers, inverse_of: :question
  has_many :claims, as: :claimable
  has_one :block, as: :blockable
  mount_uploader :image, ImageUploader
  alias_attribute :author, :user

  before_save :set_locale
  after_save :reindex_question
  scope :active, -> { where('questions.deleted = ?', false).not_blocked.order('questions.created_at desc') }
  scope :existing, -> { where(deleted: false) }
  scope :without_answer, -> { active.includes(:answers).where(:answers => {:question_id => nil}) }

  def interesting
    existingDays=(Time.now.to_i - created_at.to_i)/86400
    ((visit_counter.to_i+1)/existingDays + answers.size*10 + watchers.size*2)
  end

  def search_data
    {
        body: body,
        description: description
    }
  end

  private

  def reindex_question
    self.reindex # or reindex_async
  end

  def set_locale
    self.locale=I18n.locale
  end
end

