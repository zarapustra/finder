# -*- encoding : utf-8 -*-
class Block < ActiveRecord::Base
  belongs_to :blockable, polymorphic: true
end