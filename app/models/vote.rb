# -*- encoding : utf-8 -*-
class Vote < ActiveRecord::Base
  belongs_to :votable, polymorphic: true
  belongs_to :user

  scope :up, -> { where(positive: true) }
  scope :down, -> { where(positive: false) }
end