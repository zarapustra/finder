class Notification::QuestionAnswered < Notification::BaseNotification

  def initialize(opts = {})
    @answer = opts[:answer]
    @question = @answer.question
    @subscribers = @question.subscribers - [@answer.author]
  end

  def run
    post_to_feed   # создаем запись в ленте причастным
    notify_by_mail # присылаем письмена
  end

  def post_to_feed
    @subscribers.each do |subscriber|
      Notification::FeedService.new(user: subscriber, answer: @answer).got_answer
    end
  end

  def notify_by_mail
    @subscribers.each do |subscriber|
      Notification::QuestionAnsweredMailer.deliver_later(to: subscriber, subject: subject, answer: @answer)
    end
  end

  private

  def subject
    'На вопрос, на который вы подписались, ответили'
  end
end

