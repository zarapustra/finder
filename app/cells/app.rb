class AppCell < Cell::ViewModel
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::UrlHelper
  include Devise::Controllers::Helpers
  include ApplicationHelper
  include ActionView::Helpers::TranslationHelper
  include ActionView::Helpers::FormHelper
  #include Kaminari::Cells

end