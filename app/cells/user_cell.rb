require File.expand_path('../app.rb', __FILE__)
class UserCell < AppCell
  property :id
  property :name
  property :questions
  property :answers

  def show
    render
  end

  private

  def questions_count
    questions.length
  end

  # TODO pluralization questions
  def how_many_questions
    "#{questions_count} вопросов"
  end

  def answers_count
    answers.length
  end

  # TODO pluralization answers
  def how_many_answers
    "#{answers_count} ответов"
  end

  def item_link(text, tab, html_options)
    if @tab ==tab
      raw("<li class='chosen'>"<<link_to(text, user_path(@user, tab: tab), html_options)<<"</li>")
    else
      raw("<li>"<<link_to(text, user_path(@user, tab: tab), html_options)<<"</li>")
    end

  end
end