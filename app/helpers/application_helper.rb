# -*- encoding : utf-8 -*-
module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "Finder").join(" - ")
      end
    end
  end

  def admins_only(&block)
    block.call if current_user.try(:admin?)
  end

  def authorized?(permission, thing, &block)
    block.call if can?(permission.to_sym, thing) ||
        current_user.try(:admin?)
  end

  def signed_in_as?(user)
    user_signed_in? and current_user==user
  end

  def authenticate_admin!
    if signed_in? and !current_user.try(:admin?)
      flash[:alert] = "You must be an admin to do that."
      redirect_to root_path
    end
  end

  def signed_as_admin?
    signed_in? and current_user.try(:admin?)
  end

  def block_user_link(user)
    case user.blocked?
      when false
        concat link_to 'Блокировать', block_admin_user_path(user)
      when true
        concat link_to 'Разблокировать', unblock_admin_user_path(user)
    end
  end

  def resource_name
    :user
  end

  def resource
    @resource = session[:subscription] || User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def devise_error_messages!
    return "" if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      :count => resource.errors.count,
                      :resource => resource_name)

    html = <<-HTML
<div id="error_explanation">
<h2>#{sentence}</h2>
<ul>#{messages}</ul>
</div>
    HTML

    html.html_safe
  end
end
