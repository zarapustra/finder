class Notification::QuestionAnsweredMailer < Notification::BaseNotificationMailer
  def notify(to, subject, answer)
    @answer = answer
    I18n.with_locale(to.locale) do
      mail(to: to.email, subject: subject)
    end
  end
end
