Rails.application.routes.draw do

  scope ":locale", locale: /#{I18n.available_locales.join('|')}/ do
    devise_for :users, :controllers => {:registrations => "registrations"}
    resources :users, :only => :show
    resources :questions do
      get :mark_as_spam, :on => :member
      get :subscribe, :on => :member
      collection do
        get :autocomplete
      end
      resources :answers
    end

    resources :answers do
      get :mark_as_spam, :on => :member
      get :like, :on => :member
      get :vote_down, :on => :member
      get :vote_up, :on => :member
    end

    namespace :admin do
      get :change_spam_level, to: "base#change_spam_level"
      resources :users, :only => :show do
        get :block, on: :member
        get :unblock, on: :member
      end
      root :to => "base#index"
    end
    get :search, to: 'questions#search'
    root 'base#index'
  end

  get "*path", to: redirect("/#{I18n.locale}/%{path}")
  get "", to: redirect("/#{I18n.locale}")
end