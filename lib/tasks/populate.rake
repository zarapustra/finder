namespace :db do
  desc 'Erase and fill database'
  task :populate => :environment do
    require 'faker'
    require 'populator'

    [User, Question, Answer].each(&:delete_all)

    20.times do
      User.create(name: Faker::Name.first_name,
                    admin: false,
                    email: Faker::Internet.email,
                    password: Faker::Internet.password,
                    locale: 'ru')
    end
    Question.populate 120 do |q|
      q.body = Faker::Lorem.sentence(4)
      q.description = Faker::Lorem.sentence(4)
      q.user_id = User.offset(rand(20)).first.id
      q.deleted = false
      q.block = false
      q.locale = [:en, :ru].sample
    end
    Answer.populate 400 do |a|
      a.body = Faker::Hipster.sentence(3, true)
      a.user_id = User.offset(rand(20)).first.id
      a.question_id = Question.offset(rand(120)).first.id
      a.deleted = false
      a.block = false
    end
    Question.reindex
  end
end
